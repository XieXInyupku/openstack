# openEuler OpenStack SIG

## 使命和愿景

OpenStack是由美国国家航空航天局和Rackspace发起的，后由各大开源贡献者和厂商持续开发、维护的开源云计算软件。以Apache授权条款授权，并且是自由和开放源代码软件。

OpenStack是目前全球部署最广泛的、经过大规模生产环境验证的开源云软件，其中包括一系列软件组件，为云基础架构提供通用服务。

OpenStack社区作为著名云计算开源社区，在全球范围拥有众多个人及企业组织提供代码贡献。

openEuler OpenStack SIG致力于结合多样性算力为openstack社区贡献更适合行业发展的平台功能增强，并且定期组织会议为社区发展提供建议和回馈。

## SIG 工作目标和范围

- 在openEuler之上提供原生的OpenStack，构建开放可靠的云计算技术栈。
- 定期召开会议，收集开发者、厂商诉求，讨论OpenStack社区发展。

## 组织会议

公开的会议时间：双周例会，周三下午3:00-4:00

会议纪要： https://etherpad.openeuler.org/p/sig-openstack-meetings

## 成员

### Maintainer列表

- 陈硕[@joec88](https://gitee.com/joec88)
- 李昆山[@liksh](https://gitee.com/liksh)
- 黄填华[@huangtianhua](https://gitee.com/huangtianhua) huangtianhua223@gmail.com
- 王玺源[@xiyuanwang](https://gitee.com/xiyuanwang) wangxiyuan1007@gmail.com

### Committer列表

- 陈硕[@joec88](https://gitee.com/joec88)
- 李昆山[@liksh](https://gitee.com/liksh)
- 高松[@the-moon-is-blue](https://gitee.com/the-moon-is-blue)
- 张迎[@zhangy1317](https://gitee.com/zhangy1317)
- 黄填华[@huangtianhua](https://gitee.com/huangtianhua)
- 王玺源[@xiyuanwang](https://gitee.com/xiyuanwang)
- 请依次添加

### 联系方式

- 邮件列表：openstack@openeuler.org，邮件订阅请在[页面](https://openeuler.org/zh/community/mailing-list/)中单击OpenStack链接。
- Wechat讨论群，请联系Maintainer入群

## 本项目目录结构

```
└── templet                                       "RPM打包规范"
|   └── service-templet.spec                      "openstack服务及对应CLI打包规范"
|   └── library-templet.spec                      "依赖库打包规范"
|   └── architecture.md                           "RPM分包规则"
└── tools                                         "openstack打包、依赖分析等工作"
|   └── pyporter                                  "python库的RPM spec和软件包生成工作"
└── README.md                                     "SIG文档入口"
```

## 项目清单

### 统一入口

- https://gitee.com/openeuler/openstack

OpenStack包含项目众多，为了方便管理，设置了统一入口项目，用户、开发者对OpenStack SIG以及各OpenStack子项目有任何问题，可以在该项目中提交Issue。

### 子项目（按字母顺序）

- https://gitee.com/src-openeuler/dibbler
- https://gitee.com/src-openeuler/diskimage-builder
- https://gitee.com/src-openeuler/networking-baremetal
- https://gitee.com/src-openeuler/networking-generic-switch
- https://gitee.com/src-openeuler/novnc
- https://gitee.com/src-openeuler/openstack-cinder
- https://gitee.com/src-openeuler/openstack-glance
- https://gitee.com/src-openeuler/openstack-horizon
- https://gitee.com/src-openeuler/openstack-ironic
- https://gitee.com/src-openeuler/openstack-ironic-inspector
- https://gitee.com/src-openeuler/openstack-ironic-python-agent
- https://gitee.com/src-openeuler/openstack-ironic-python-agent-builder
- https://gitee.com/src-openeuler/openstack-ironic-staging-drivers
- https://gitee.com/src-openeuler/openstack-keystone
- https://gitee.com/src-openeuler/openstack-macros
- https://gitee.com/src-openeuler/openstack-neutron
- https://gitee.com/src-openeuler/openstack-nova
- https://gitee.com/src-openeuler/openstack-placement
- https://gitee.com/src-openeuler/openstack-swift
- https://gitee.com/src-openeuler/openstack-tempest
- https://gitee.com/src-openeuler/python-automaton
- https://gitee.com/src-openeuler/python-barbicanclient
- https://gitee.com/src-openeuler/python-castellan
- https://gitee.com/src-openeuler/python-cinderclient
- https://gitee.com/src-openeuler/python-cliff
- https://gitee.com/src-openeuler/python-cursive
- https://gitee.com/src-openeuler/python-debtcollector
- https://gitee.com/src-openeuler/python-designateclient
- https://gitee.com/src-openeuler/python-dracclient
- https://gitee.com/src-openeuler/python-etcd3gw
- https://gitee.com/src-openeuler/python-futurist
- https://gitee.com/src-openeuler/python-glanceclient
- https://gitee.com/src-openeuler/python-glance-store
- https://gitee.com/src-openeuler/python-hacking
- https://gitee.com/src-openeuler/python-heatclient
- https://gitee.com/src-openeuler/python-ironicclient
- https://gitee.com/src-openeuler/python-ironic-inspector-client
- https://gitee.com/src-openeuler/python-ironic-lib
- https://gitee.com/src-openeuler/python-keystoneauth1
- https://gitee.com/src-openeuler/python-keystoneclient
- https://gitee.com/src-openeuler/python-keystonemiddleware
- https://gitee.com/src-openeuler/python-ldappool
- https://gitee.com/src-openeuler/python-microversion-parse
- https://gitee.com/src-openeuler/python-netmiko
- https://gitee.com/src-openeuler/python-neutronclient
- https://gitee.com/src-openeuler/python-neutron-lib
- https://gitee.com/src-openeuler/python-novaclient
- https://gitee.com/src-openeuler/python-openstackclient
- https://gitee.com/src-openeuler/python-openstackdocstheme
- https://gitee.com/src-openeuler/python-openstacksdk
- https://gitee.com/src-openeuler/python-osc-lib
- https://gitee.com/src-openeuler/python-osc-placement
- https://gitee.com/src-openeuler/python-oslotest
- https://gitee.com/src-openeuler/python-oslo.cache
- https://gitee.com/src-openeuler/python-oslo.concurrency
- https://gitee.com/src-openeuler/python-oslo.config
- https://gitee.com/src-openeuler/python-oslo.context
- https://gitee.com/src-openeuler/python-oslo.db
- https://gitee.com/src-openeuler/python-oslo.i18n
- https://gitee.com/src-openeuler/python-oslo.log
- https://gitee.com/src-openeuler/python-oslo.messaging
- https://gitee.com/src-openeuler/python-oslo.middleware
- https://gitee.com/src-openeuler/python-oslo.policy
- https://gitee.com/src-openeuler/python-oslo.privsep
- https://gitee.com/src-openeuler/python-oslo.reports
- https://gitee.com/src-openeuler/python-oslo.rootwrap
- https://gitee.com/src-openeuler/python-oslo.serialization
- https://gitee.com/src-openeuler/python-oslo.service
- https://gitee.com/src-openeuler/python-oslo.sphinx
- https://gitee.com/src-openeuler/python-oslo.upgradecheck
- https://gitee.com/src-openeuler/python-oslo.utils
- https://gitee.com/src-openeuler/python-oslo.versionedobjects
- https://gitee.com/src-openeuler/python-oslo.vmware
- https://gitee.com/src-openeuler/python-osprofiler
- https://gitee.com/src-openeuler/python-os-brick
- https://gitee.com/src-openeuler/python-os-client-config
- https://gitee.com/src-openeuler/python-os-ken
- https://gitee.com/src-openeuler/python-os-resource-classes
- https://gitee.com/src-openeuler/python-os-service-types
- https://gitee.com/src-openeuler/python-os-testr
- https://gitee.com/src-openeuler/python-os-traits
- https://gitee.com/src-openeuler/python-os-vif
- https://gitee.com/src-openeuler/python-os-win
- https://gitee.com/src-openeuler/python-os-xenapi
- https://gitee.com/src-openeuler/python-ovsdbapp
- https://gitee.com/src-openeuler/python-proliantutils
- https://gitee.com/src-openeuler/python-pycadf
- https://gitee.com/src-openeuler/python-pyghmi
- https://gitee.com/src-openeuler/python-reno
- https://gitee.com/src-openeuler/python-requestsexceptions
- https://gitee.com/src-openeuler/python-requests-mock
- https://gitee.com/src-openeuler/python-scciclient
- https://gitee.com/src-openeuler/python-sqlalchemy-migrate
- https://gitee.com/src-openeuler/python-stestr
- https://gitee.com/src-openeuler/python-stevedore
- https://gitee.com/src-openeuler/python-sushy
- https://gitee.com/src-openeuler/python-swiftclient
- https://gitee.com/src-openeuler/python-taskflow
- https://gitee.com/src-openeuler/python-textfsm
- https://gitee.com/src-openeuler/python-tooz
- https://gitee.com/src-openeuler/python-websockify
- https://gitee.com/src-openeuler/python-wsme
- https://gitee.com/src-openeuler/python-XStatic-Angular
- https://gitee.com/src-openeuler/python-XStatic-Angular-Bootstrap
- https://gitee.com/src-openeuler/python-XStatic-Angular-FileUpload
- https://gitee.com/src-openeuler/python-XStatic-Angular-Gettext
- https://gitee.com/src-openeuler/python-XStatic-Angular-lrdragndrop
- https://gitee.com/src-openeuler/python-XStatic-Angular-Schema-Form
- https://gitee.com/src-openeuler/python-XStatic-Bootstrap-Datepicker
- https://gitee.com/src-openeuler/python-XStatic-Bootstrap-SCSS
- https://gitee.com/src-openeuler/python-XStatic-bootswatch
- https://gitee.com/src-openeuler/python-XStatic-D3
- https://gitee.com/src-openeuler/python-XStatic-Font-Awesome
- https://gitee.com/src-openeuler/python-XStatic-Hogan
- https://gitee.com/src-openeuler/python-XStatic-Jasmine
- https://gitee.com/src-openeuler/python-XStatic-jquery-ui
- https://gitee.com/src-openeuler/python-XStatic-jQuery
- https://gitee.com/src-openeuler/python-XStatic-JQuery.quicksearch
- https://gitee.com/src-openeuler/python-XStatic-JQuery.TableSorter
- https://gitee.com/src-openeuler/python-XStatic-JQuery-Migrate
- https://gitee.com/src-openeuler/python-XStatic-JSEncrypt
- https://gitee.com/src-openeuler/python-XStatic-mdi
- https://gitee.com/src-openeuler/python-XStatic-objectpath
- https://gitee.com/src-openeuler/python-XStatic-Rickshaw
- https://gitee.com/src-openeuler/python-XStatic-roboto-fontface
- https://gitee.com/src-openeuler/python-XStatic-smart-table
- https://gitee.com/src-openeuler/python-XStatic-Spin
- https://gitee.com/src-openeuler/python-XStatic-term.js
- https://gitee.com/src-openeuler/python-XStatic-tv4
